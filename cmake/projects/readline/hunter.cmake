# Copyright (c) 2016-2019, Ruslan Baratov
# All rights reserved.

# !!! DO NOT PLACE HEADER GUARDS HERE !!!

include(hunter_add_version)
include(hunter_cacheable)
include(hunter_cmake_args)
include(hunter_configuration_types)
include(hunter_download)
include(hunter_pick_scheme)

hunter_add_version(
    PACKAGE_NAME
    readline
    VERSION
    "6.3"
    URL
    "https://ftp.gnu.org/gnu/readline/readline-6.3.tar.gz"
    SHA1
    017b92dc7fd4e636a2b5c9265a77ccc05798c9e1
)

hunter_add_version(
    PACKAGE_NAME
    readline
    VERSION
    "8.1"
    URL
    "https://fossies.org/linux/misc/readline-8.1.tar.gz"
    SHA1
    48c5e0c3a212449e206e21ba82856accac842c6f
)

hunter_add_version(
    PACKAGE_NAME
    readline
    VERSION
    "8.1.2"
    URL
    "https://fossies.org/linux/misc/readline-8.1.2.tar.gz"
    SHA1
    8A05AD0D0AD67E18C383F1B2CF6A23BCBD46F87A
)

hunter_add_version(
    PACKAGE_NAME
    readline
    VERSION
    "8.2"
    URL
    "https://fossies.org/linux/misc/readline-8.2.tar.gz"
    SHA1
    97ad98be243a857b639c0f3da2fe7d81c6d1d36e
)



hunter_configuration_types(readline CONFIGURATION_TYPES Release)
hunter_pick_scheme(DEFAULT url_sha1_autotools)
hunter_cmake_args(
    readline
    CMAKE_ARGS
      DEPENDS_ON_PACKAGES=ncursesw
)
hunter_cacheable(readline)
hunter_download(
    PACKAGE_NAME readline
)

